import socket

target_host = str(input("host: ").strip())
target_port = int(input("port").strip())

# create a socket
client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# connect the client
client.connect((target_host, target_port))

# send data
client.send(b"GET / HTTP/1.1\r\rHost: google.com\r\n\r\n")

# receive data
response = client.recv(4096)

print(response.decode())
client.close()
